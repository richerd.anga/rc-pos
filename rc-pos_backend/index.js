//forma convencional de ES6 > estandar convencional de java Script
//import express from express
//forma sistema nativo node js
const express= require('express');
/* const { Mongoose } = require('mongoose'); */

const router= express.Router();

// importar modulo de MongoDB > mongoose

//const mongoose = require('mongoose');

//IMPORTAR  LAS VARIABLES DE ENTORNO
//require('dotenv').config({path: 'var.env'})

//importar modulo a la base de datos (archivo db.js)
const conectarDB= require('./config/db'); 

let app= express();

//app.use('/',function(req, res){
//    res.send("hola tripulantes")
//});
app.use(router);

//conexion a la base de datos
conectarDB();


//console.log(process.env.URL_MONGODB);
//mongoose.connect(process.env.URL_MONGODB)
   //.then(function(){console.log("conexion establecida con mongoDB Atlas")})
   //.catch(function(e){console.log(e)})

//modelo esquema (schema) de la base de datos

/* const productoSchema = new mongoose.Schema({
    id:Number,
    nombre:String,
    cantidad: Number,
    categoria: String,
    precio: Number
})*/

//modelo del producto => tener en cuenta 
//el nombre de coleccion y el esquema 
//const modeloProducto = mongoose.model('productos',productoSchema);


//CRUD => CREATE

//modeloProducto.create(
//{
  //  id:1,
  //  nombre: "lomo de res",
    //cantidad: 12,
    //categoria: "carnes",
   // precio: 20000
//},
//(error) => {
 //   console.log("ingreso a funcion error");
   // if (error) return console.log(error);
    //console.log("sale de la funcion error");
//});

 /*CRUD => READ
 modeloProducto.find((error, productos)=>{
    if(error) return console.log(error);
    console.log(productos)
});*/


//CRUD => UPDATE
/* modeloProducto.updateOne({id: 1}, {id: 11, nombre:"costillitas", cantidad:32}, (error)=>{
    if(error) return console.log(error);
});
 */
//CRUD => DELETE
/* modeloProducto.deleteOne({cantidad: 12}, (error)=>{
if(error)return console.log(error);
}); */

//---------------------------------------------------
// ############ Descentralizacion del Crud ##########
// ############       Rutas al  Crud       ##########
//---------------------------------------------------

//uso de archivos tipo json 
app.use(express.json());

//CORS => Mecanismos  o reglas de seguridad para el control de peticiones http
const cors =require('cors');

//solicitudes al crud
const crudProductos = require('./controllers/controlProducts');

//Establecer las rutas respecto al Crud
// Crud => Create
router.post('/', crudProductos.crear);
// Crud => Read
router.get('/', crudProductos.obtener);
// Crud => Update
router.put('/:id', crudProductos.actualizar);
// Crud => Delete
router.delete('/:id', crudProductos.eliminar);




//router.get('/metodoget', function(req, res){
//res.send("utilizando el metodo get...")
//conectarDB();
//});
/* router.post('/metodoget',function(req, res){
res.send("utilizando el metodo post...")
}); */

router.post('/postmetodo',function(req, res){
    res.send("ruta /postmetodo")
   // const user='b30xx';
   // const psw='b30gxx123';
   // const db='RC-POS';
   // const url=`mongodb+srv://${user}:${psw}@proyectociclo4.eumuw.mongodb.net/${db}?retryWrites=true&w=majority`;
    
   // mongoose.connect(url)
    //    .then(function(){console.log("conexion establecida con mongoDB Atlas")})
     //   .catch(function(e){console.log(e)})

    });

//conexion con la base de datos
//const user='b30xx';
//const psw='b30gxx123';
//const db='RC-POS';
///const url=`mongodb+srv://${user}:${psw}@proyectociclo4.eumuw.mongodb.net/${db}?retryWrites=true&w=majority`;

//mongoose.connect(url)
    //.then(function(){console.log("conexion establecida con mongoDB Atlas")})
    //.catch(function(e){console.log(e)})



app.listen(4000);

console.log("la aplicacion se ejecuta en http://localhost:4000");

