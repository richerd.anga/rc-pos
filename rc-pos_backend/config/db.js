// importar modulo de MongoDB > mongoose

const mongoose = require('mongoose');

//IMPORTAR  LAS VARIABLES DE ENTORNO
require('dotenv').config({path: 'var.env'})

const conexionDB = async () =>{
    try {
        await  mongoose.connect(process.env.URL_MONGODB,{})
        console.log("conexion establecida con mongodb atlas desde archivo db.js");
    } catch (error) {
        console.log("Error de conexion a la base de datos");
        console.log(error);
        process.exit(1);
    }

} 

//exportar como modulo para utilizar en otros archivos
module.exports = conexionDB;