// importar el modelo de la base de datos para el crud
const modeloProducto = require('../models/modelProductos');


//Exportar diferentes variables, los metodos para el crud

//CRUD => CREATE

exports.crear = async (req, res)=>{
try {
    let producto;

    producto= new modeloProducto({
        id:12,
        nombre: "lomo de res",
        cantidad: 12,
        categoria: "carnes",
        precio: 20000
    });
    await producto.save();
    res.send(producto);
} catch (error) {
    console.log(error);
    res.status(500).send('ERROR AL GUARDAR EL PRODUCTO.'); 
}

}

//CRUD => READ

exports.obtener = async (req, res) => {
    try {
        const producto = await modeloProducto.find();
        res.json(producto);
    } catch (error) {
        console.log(error);
        res.status(500).send('error al obtener el/ los producto(s).');
    }

}

//CRUD => UPDATE

exports.actualizar = async (req, res) =>{
    try {
        const producto = await modeloProducto.findById(req.params.id);
        if(!producto){
            console.log(producto);
            res.status(404).json({msg: 'El producto no existe'});
        }
        else{
            await modeloProducto.findByIdAndUpdate({_id: req.params.id},{cantidad: 66});
            res.json({msg: 'producto actualizado corerctamente'});
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al editar el producto.');
    }

}


//CRUD => DELETE

exports.eliminar = async (req, res) =>{
    try {
        const producto = await modeloProducto.findById(req.params.id);
        if(!producto){
            console.log(producto);
            res.status(404).json({msg: 'El producto no existe'});
        }
        else{
            await modeloProducto.findByIdAndRemove({_id: req.params.id});
            res.json({mensaje: 'producto eliminado correctamente'})
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('error al eliminar el producto');
    }

}
